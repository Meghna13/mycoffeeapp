import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getList(callback) {
    //TODO: to Change with real webervice
    const List = [
      new Coffee('Double Espresso', 'Sani Caffe', new PlaceLocation('123 Market Stree', 'California')),
      new Coffee('Americano Espresso', 'Sunny Caffe', new PlaceLocation('456 Market Stree', 'Madrid'))
    ];
    callback(List);
  }

  save(coffee,callback){
    //TODO: to Change with real webervice
    callback(true);
  }
}
